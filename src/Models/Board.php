<?php

namespace Zunderdorp\Models;

/**
 * Class Board
 * @package Zunderdorp\Models
 */
class Board
{
    const SIZE_OF_SIDES = 7;
    const SENTINEL = null;
    const QUEEN = true;
    const BLOCKED_BY_QUEEN = false;

    /**
     * @var int
     */
    protected $horizontalSize = self::SIZE_OF_SIDES;

    /**
     * @var int
     */
    protected $verticalSize = self::SIZE_OF_SIDES;

    /**
     * @var int
     */
    protected $queens = 0;

    /**
     * @var array|null
     */
    protected $board = [];

    /**
     * Board constructor.
     */
    public function __construct()
    {
        if (empty($board)) {
            for ($x = 0; $x < $this->horizontalSize; $x++) {
                $this->board[$x] = [];
                for ($y = 0; $y < $this->verticalSize; $y++) {
                    $this->board[$x][$y] = self::SENTINEL;
                }
            }
        }
    }

    /**
     * @return int
     */
    public function getHorizontalSize(): int
    {
        return $this->horizontalSize;
    }

    /**
     * @return int
     */
    public function getVerticalSize(): int
    {
        return $this->verticalSize;
    }

    /**
     * @return bool
     */
    public function isSquare(): bool
    {
        return $this->verticalSize === $this->horizontalSize;
    }

    /**
     * @param int $x
     * @param int $y
     * @return bool|null
     * @throws \OutOfBoundsException
     */
    public function getPosition(int $x, int $y)
    {
        if (array_key_exists($x, $this->board) && array_key_exists($y, $this->board[$x])) {
            return $this->board[$x][$y];
        }
        throw new \OutOfBoundsException();
    }

    /**
     * @return int
     */
    public function getQueens(): int
    {
        return $this->queens;
    }

    /**
     * @param \Zunderdorp\Models\Board $board
     * @return bool
     */
    public function isDuplicateOf(Board $board): bool
    {
        for ($x = 0; $x < $this->horizontalSize; $x++) {
            for ($y = 0; $y < $this->verticalSize; $y++) {
                if ($board->getPosition($x, $y) !== $this->getPosition($x, $y)) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @param int $x
     * @param int $y
     * @return bool
     * @throws \OutOfBoundsException
     */
    public function placeQueen(int $x, int $y)
    {
        if (array_key_exists($x, $this->board) && array_key_exists($y, $this->board[$x])) {
            if ($this->board[$x][$y] === self::SENTINEL) {
                /**
                 * Inspired by the sieve of eratosthenes for finding prime numbers,
                 * we use the same technique to filter out other positions.
                 */
                $this->blockPositionsThatQueenCanHit($x, $y);

                $this->board[$x][$y] = self::QUEEN;
                $this->queens++;

                return self::QUEEN;
            }
            return $this->board[$x][$y];
        }
        throw new \OutOfBoundsException();
    }

    /**
     * @param int $x
     * @param int $y
     * @return bool
     */
    public function canPlaceQueen(int $x, int $y): bool
    {
        return array_key_exists($x, $this->board) &&
            array_key_exists($y, $this->board[$x]) &&
            $this->board[$x][$y] === self::SENTINEL;
    }

    /**
     * Blocks all positions that the newly placed queen can hit
     *
     * @param int $x horizontal position of queen
     * @param int $y vertical position of queen
     */
    protected function blockPositionsThatQueenCanHit(int $x, int $y)
    {
        for ($i = 1; $i <= max($this->horizontalSize, $this->verticalSize); $i++) {
            $this->blockPosition($x + $i, $y);
            $this->blockPosition($x - $i, $y);
            $this->blockPosition($x, $y + $i);
            $this->blockPosition($x, $y - $i);
            $this->blockPosition($x + $i, $y + $i);
            $this->blockPosition($x + $i, $y - $i);
            $this->blockPosition($x - $i, $y + $i);
            $this->blockPosition($x - $i, $y - $i);
        }
    }

    /**
     * @param int $x
     * @param int $y
     */
    protected function blockPosition(int $x, int $y)
    {
        if (array_key_exists($x, $this->board) && array_key_exists($y, $this->board[$x])) {
            $this->board[$x][$y] = self::BLOCKED_BY_QUEEN;
        }
        // Soft fail for blockPositionsThatQueenCanHit
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        $output = '';

        /**
         * Lines are rendered from top to bottom, so render top row first
         */
        for ($y = $this->verticalSize - 1; $y >= 0; $y--) {
            for ($x = 0; $x < $this->horizontalSize; $x++) {
                $value = $this->getPosition($x, $y);

                if ($value === self::SENTINEL) {
                    $output .= '.';
                } elseif ($value == self::QUEEN) {
                    $output .= 'Q';
                } elseif ($value === self::BLOCKED_BY_QUEEN) {
                    $output .= 'X';
                }
            }

            $output .= PHP_EOL;
        }

        return $output;
    }
}