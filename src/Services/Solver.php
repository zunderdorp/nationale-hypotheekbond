<?php

namespace Zunderdorp\Services;

use Zunderdorp\Models\Board;

/**
 * Class Solver
 * @package Zunderdorp\Solver
 */
class Solver
{
    const REQUIRED_QUEENS = 7;

    /**
     * @return array of unique boards
     * @throws \Exception
     */
    public static function solve(): array
    {
        $board = new Board();

        $uniqueSolutions = [];

        if ($board->isSquare()) {
            for ($x = 0; $x < $board->getHorizontalSize(); $x++) {
                $board->placeQueen($x, 0);
                $solutionsForInitialBoard = self::attemptToPlaceQueens($board, 0);

                /** @var Board $solution */
                foreach ($solutionsForInitialBoard as $solution) {
                    $isUnique = true;
                    /** @var Board $acceptedSolution */
                    foreach ($uniqueSolutions as $acceptedSolution) {
                        if ($solution->isDuplicateOf($acceptedSolution)) {
                            $isUnique = false;
                            break;
                        }
                    }

                    if ($isUnique) {
                        $uniqueSolutions[] = $solution;
                    }
                }
            }
        } else {
            throw new \Exception('Non-square boards support not implemented yet');
        }

        return $uniqueSolutions;
    }

    /**
     * @param \Zunderdorp\Models\Board $board
     * @param int $lastPlacedHeight
     * @return array
     */
    public static function attemptToPlaceQueens(Board $board, int $lastPlacedHeight): array
    {
        $solutions = [];
        for ($x = 0; $x < $board->getHorizontalSize(); $x++) {
            for ($y = $lastPlacedHeight + 1; $y < $board->getVerticalSize(); $y++) {
                /**
                 * We always work bottoms up, because any possible placements below the last placement is already filled
                 * in alternative solutions
                 */

                if ($board->canPlaceQueen($x, $y)) {
                    $nextBoard = clone $board;
                    $nextBoard->placeQueen($x, $y);

                    if ($nextBoard->getQueens() === self::REQUIRED_QUEENS) {
                        $solutions[] = $nextBoard;
                    } else {
                        // Implicit $queens < required
                        $solutions = array_merge($solutions, self::attemptToPlaceQueens($nextBoard, $y));
                    }
                }
            }
        }

        return $solutions;
    }
}