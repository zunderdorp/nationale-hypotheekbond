# Nationale Hypotheekbond
## Programming challenge "Schaakbord" (Chessboard)

### Requirements
* PHP 7.0
* Composer

### Installation
* ```composer install```

### Running
* ```composer run program```

### Theory behind solution
This challenge is a variation on sudoku: A queen can only be placed once in every column, row AND diagonal.

The solution has been engineered with the following assumptions for now & the future:
* There are no other pieces to take in account
* A solution is considered an unique solution if there is no other solution that is identical, is a rotation of or is a mirror of the current solution

Additionally, there are a couple of assumptions about the growth path:
* The board size can change, as can the number of required queens.
* Taking in account non-square boards is not relevant at this time, but likely to be implemented in the future
* Memory consumption is not a huge concern now, but can be optimized later

Based on the assumptions above, I have taken inspiration from the algorithm "Sieve of Eratosthenes". Every position taken by a queen directly invalidates all other positions in the same row/column/diagonal. This allows us to only look into fields of which we know they are not blocked yet by other queens, without re-calculating every possible conflict per iteration.
However, instead of using a boolean array, I opted for a three-valued approach in order to be able to visualize results at any point during the calculation (also aiding debugging). If this is not required, the logic can somewhat be simplified.
