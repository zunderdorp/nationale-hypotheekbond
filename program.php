<?php

require __DIR__ . '/vendor/autoload.php';

use Zunderdorp\Services\Solver;

echo "The 7 queens problem solver" . PHP_EOL;

$solutions = (new Solver())->solve();

printf('There are %d unique solutions that are not mirrors or rotations of each other', count($solutions));
$legend = <<<LEGEND
Legend:
. = empty (should not occur)
Q = queen
X = blocked by any other queen
LEGEND;

print PHP_EOL . PHP_EOL . $legend . PHP_EOL . PHP_EOL;

foreach ($solutions as $i => $solution) {
    printf('Solution %d', $i + 1);
    print PHP_EOL;
    print $solution;
    print PHP_EOL;
}